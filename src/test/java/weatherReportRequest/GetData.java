package weatherReportRequest;

import java.util.Properties;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;

import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

import java.io.FileInputStream;

import java.io.IOException;
import java.util.ArrayList;

public class GetData {

	Properties property;
	FileInputStream inputStrm;
	String basUrl;

	String weatrUrl;

	String appid;

	@BeforeTest
	public void setproperty() throws IOException {

		inputStrm = new FileInputStream(System.getProperty("user.dir") + "\\config.properties");

		property = new Properties();

		property.load(inputStrm);

		basUrl = (String) property.get("url");

		weatrUrl = (String) property.get("weatherurl");

		appid = (String) property.get("appid");

		System.out.println(appid);

	}

	@Parameters({ "citi" })
	@Test(description="Validating city name is not unique and response will come asa list")
	public void city_Not_Unique(String citi) {

		RestAssured.baseURI = basUrl;

		Response respns = given().contentType(ContentType.JSON).get(citi + appid);

		ArrayList<String> abc = new ArrayList<String>();

		int count = respns.then().extract().path("count");

		System.out.println(count);

		// Validating city name is not unique and it is a list

		if (count > 1) {

			System.out.println("City Name is not unique, its having list of  " + count);
		}

		// Fetching all the country code in which given city is present
		for (int i = 0; i < count; i++) {

			String Coun = respns.then().extract().path("list.sys.country[" + i + "]");

			System.out.println(Coun);
			abc.add(Coun);
		}

		// Listing all the country code in which given city is present
		for (String countyCode : abc)

		{

			System.out.println("Different contry codes for this city " + countyCode);
		}
	}

	@Parameters({ "citi", "CounCode" })
	@Test (description="Get current weather for a city and country code and validate that country and id is correct")
	public void testCountryId(String citi, String CounCode) {
		// Scenario to validate validate that country and id is correct
		System.out.println(citi);
		System.out.println(CounCode);

		RestAssured.baseURI = weatrUrl;

		Response respns = given().contentType(ContentType.JSON).get(citi + CounCode + appid);

		// Fetching Status code from response
		int code = respns.getStatusCode();

		System.out.println(code);

		// Validating Status Code is coming correct or not

		Assert.assertEquals(code, 200);

		// Fetching Country code from response
		String name = respns.then().extract().path("name");

		// Fetching id from response
		int id = respns.then().extract().path("id");

		System.out.println("Name of the city" + name);

		System.out.println("Name of the city" + id);

		// validate that country and id is correct

		Assert.assertEquals(name, "Lucknow", "Correct id received in the Response");

		Assert.assertEquals(id, 1264733, "Correct id received in the Response");

	}

	@Parameters({ "citi", "CounCode" })
	@Test (description="Passing Invalid City and expecting 404 status code")
	public void citiNotFound(String citi, String CounCode) {

		RestAssured.baseURI = weatrUrl;

		// Passing Invalid City and expecting 404 status code
		Response respns = given().contentType(ContentType.JSON).get(citi + "abc" + CounCode + appid);

		// Fetching Status code from response
		int code = respns.getStatusCode();

		System.out.println(code);

		// Validating Status Code
		Assert.assertEquals(code, 404);

		String errorMessage = given().contentType(ContentType.JSON).get(citi + "abc" + CounCode + appid).then()
				.extract().path("message");

		// Validating Response message
		Assert.assertEquals(errorMessage, "city not found", "City not found which you search");

	}

	@Parameters({ "citi", "CounCode" })
	@Test (description="Passing Invalid Appid and expecting 401 status code")
	public void invalid_API_key(String citi, String CounCode) {

		RestAssured.baseURI = weatrUrl;

		// Passing Invalid Appid and expecting 401 status code
		Response respns = given().contentType(ContentType.JSON).get(citi + CounCode + appid + "ab");

		// Fetching Status code from response
		int code = respns.getStatusCode();

		System.out.println(code);

		// Validating Status Code
		Assert.assertEquals(code, 401);

		String errorMessage = respns.then().extract().path("message");

		// Validating Response message
		Assert.assertEquals(errorMessage,
				"Invalid API key. Please see http://openweathermap.org/faq#error401 for more info.",
				"API key is not valid");

	}

}
